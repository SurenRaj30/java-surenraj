<%
    String n = request.getParameter("number");
    double number = Double.parseDouble(request.getParameter("number"));
    String positive = "positive ";
    String[] check_decimal = n.split("\\.");

    if(check_decimal.length == 1) {

    } else {
         if(check_decimal[1].length() > 4) {
            n = String.format("%.4f", number);
         }
    }

    String[] arr = n.split("");
    String[] nStr = new String[arr.length];

    for(int i = 0; i<arr.length; i++) {
       if(arr[i].equals("0")) {
           nStr[i] = "Zero";
       } else if(arr[i].equals("1")) {
           nStr[i] = "One";
       } else if (arr[i].equals("2")) {
           nStr[i] = "Two";
       } else if (arr[i].equals("3")) {
           nStr[i] = "Three";
       } else if (arr[i].equals("4")) {
           nStr[i] = "Four";
       } else if (arr[i].equals("5")) {
           nStr[i] = "Five";
       } else if (arr[i].equals("6")) {
           nStr[i] = "Six";
       } else if (arr[i].equals("7")) {
           nStr[i] = "Seven";
       } else if (arr[i].equals("8")) {
           nStr[i] = "Eight";
       } else if (arr[i].equals("9")) {
           nStr[i] = "Nine";
       } else if (arr[i].equals(".")) {
           nStr[i] = "point";
       } else if (arr[i].equals("-")) {
            if(number == 0) {
               nStr[i] = "";
            } else {
                nStr[i] = "negative";
            }
       }
    }

    StringBuilder sb = new StringBuilder();
    for(int j = 0; j<nStr.length; j++) {
        sb.append(nStr[j]).append(" ");
    }

    String result = null;

    if(number > 0) {
        result = positive + sb.toString();
    } else {
        result = sb.toString();
    }
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
     rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Result</title>
</head>
<body>
   <div style="display: flex; justify-content: center;">
       <div style="text-align: center;">
           <h1>String Version</h1><br><br>
           <input type="text" name="number" value="<%= result %>" style="width: 300px;"><br><br>
           <a href="calculate_form.jsp">Home</a>
       </div>
   </div>
</body>
</html>

