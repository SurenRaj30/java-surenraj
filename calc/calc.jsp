<%@ page errorPage="jsperror.jsp" %>
<%

    String opt = request.getParameter("opt");
    double num1 = Double.parseDouble(request.getParameter("num1"));
    double num2 = Double.parseDouble(request.getParameter("num2"));

    double result = 0;
    String result_opt = "";

    switch(opt) {
        case "+":
            result = num1 + num2;
            result_opt = "Addition";
            break;

        case "-":
            result = num1 - num2;
            result_opt = "Subtraction";
            break;

        case "/":
            if (num2==0) {
                response.sendRedirect("calculate_form.jsp");
            }
            result = num1 / num2;
            result_opt = "Division";
            break;

        case "*":
            result = num1 * num2;
            result_opt = "Multiplication";
            break;

        default:
            response.sendRedirect("calculate.jsp");
            break;
    }

    String str = Double.toString(result);
    String[] check_decimal = str.split("\\.");

    if(check_decimal[1].length() == 1) {
        if(check_decimal[1].equals("0")) {
            str = String.format("%.0f", result);
        }
   }


%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
     rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Result</title>
</head>
<body>
       <div style="display: flex; justify-content: center;">
           <div style="text-align: center;">
               <h1><%= result_opt %></h1>
               <form method="post" action="string.jsp">
                   <input type="number" name="number" value="<%= str %>"><br><br>
                   <input type="submit" value="String Version">
               </form>
           </div>
       </div>
</body>
</html>