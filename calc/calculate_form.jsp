<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
    rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Calculate</title>
</head>
<body>
    <div style="display: flex; justify-content: center;">
        <div style="text-align: center;">
            <form method="post" action="calc.jsp" name="form">
                <h2>ENTER DATA:</h2><br><br>
                <input type="number" name="num1" step="any"required><br><br>
                <input type="number" name="num2" step="any" required><br><br>

                <select name="opt">
                    <option value="+">Addition</option>
                    <option value="-">Subtraction</option>
                    <option value="/">Division</option>
                    <option value="*">Multiplication</option>
                </select>
                <input type="submit" value="Calculate"/>
            </form>
        </div>
    </div>
</body>
</html>